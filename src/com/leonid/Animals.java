package com.leonid;

/**
 * Created by LEONID on 23.10.2014.
 */
public abstract class Animals {
    public static final String OUTPUT_FORMAT_LINE = "%s говорит '%s'.";
    private String name;
    private String voice;

    protected Animals(String name, String voice)
    {
        this.name = name;
        this.voice = voice;
    }

    /**
     * метод возвращает название животного
     * @return (String) название животного
     */
    public String getName() {
        return name;
    }

    /**
     * Метод, возвращает голос (слова/говор) животного
     * @return (String) голос (слова/говор) животного
     */
    public String getVoice() {
        return voice;
    }
    /**
     * Выводит сообщение по заданому формату
     */
    public void printDisplay(){
        System.out.println(String.format(OUTPUT_FORMAT_LINE, name, voice));
    }
}
