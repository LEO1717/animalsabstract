package com.leonid;

/**
 * Created by LEONID on 23.10.2014.
 */
public class Main {
    public static void main(String[] args) {
        Dog dog =new Dog();
        Dog dog1 = new Dog("Тузик", "тяф-тяф");
        Dog dog2 = new Dog("Собака", "гав-гав");

        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();
    }
}
