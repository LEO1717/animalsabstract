package com.leonid;

/**
 * Created by LEONID on 23.10.2014.
 */
public class Dog extends Animals{

    public Dog(){
        this("Собака", "гав-гав");
    }

    protected Dog(String name, String voice) {
        super(name, voice);
    }
}
